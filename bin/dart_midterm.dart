import 'package:dart_midterm/dart_midterm.dart' as dart_midterm;
import 'dart:io';
import 'dart:math';

String num_token = '123*+6';
void main(List<String> arguments) {
  ex1token();
  ex2infixtopostfix();
  ex3evaofpostfix();
}
//exercise 1
void ex1token() {
  var token = num_token.split(' '); 
  for (int i = 0; i < token.length; i++) {
    token.remove(''); 
  }
  print('Token_ans : $token ');
}
//exercise 2
void ex2infixtopostfix() {
  var pi = List.empty(growable: true); 
  var pa = List.empty(growable: true); 
  var ch = '';
  var s, t;
  for (var i = 0; i < num_token.length; i++) {
    if (num_token[i] == '0' ||
        num_token[i] == '1' ||
        num_token[i] == '2' ||
        num_token[i] == '3' ||
        num_token[i] == '4' ||
        num_token[i] == '5' ||
        num_token[i] == '6' ||
        num_token[i] == '7' ||
        num_token[i] == '8' ||
        num_token[i] == '9') { 
      pi.add(num_token[i]);
    } else if (num_token[i] == '+' ||
        num_token[i] == '-' ||
        num_token[i] == '*' ||
        num_token[i] == '/' ||
        num_token[i] == '%') {
      if (num_token[i] == '(' || num_token == ')') {
        s = 0;
      } else if (num_token[i] == '+' || num_token[i] == '-') {
        s = 1;
      } else if (num_token[i] == '*' || num_token[i] == '/') {
        s = 2;
      } else {
        s = 3;
      }
      if (pa.length > 0) {
        if (pa.last == '(' || pa.last == ')') {
          t = 0; 
        } else if (pa.last == '+' || pa.last == '-') {
          t = 1; 
        } else if (pa.last == '*' || pa.last == '/') {
          t = 2;
        } else {
          t = 3; 
        }
      }
      while (pa.isNotEmpty && pa.last != '(' && s <= t) {
        String keep = pa.last;
        pa.removeLast();
        pi.add(keep);
      }
      pa.add(num_token[i]);
    }
    if (num_token[i] == '(') {
      pi.add(num_token);
    } else if (num_token[i] == ')') {
      while (pi.last != '(') {
        String keep = pi.last;
        pi.removeLast();
        pa.add(keep);
      }
      pi.remove('(');
    }
  }
  while (pa.isNotEmpty) {
    String keep = pa.last;
    pa.removeLast();
    pi.add(keep);
  }
  print('Infix to postfix : $pi');
}
//exercise 3
void ex3evaofpostfix() {
  var pi = List.empty(growable: true); 
  double num;
  for (int i = 0; i < num_token.length; i++) {
    if (num_token[i] == "0" ||
        num_token[i] == "1" ||
        num_token[i] == "2" ||
        num_token[i] == "3" ||
        num_token[i] == "4" ||
        num_token[i] == "5" ||
        num_token[i] == "6" ||
        num_token[i] == "7" ||
        num_token[i] == "8" ||
        num_token[i] == "9") {
      switch (num_token[i]) {
        case "0":
          {
            num = 0;
            pi.add(num);
          }
          break;
        case "1":
          {
            num = 1;
            pi.add(num);
          }
          break;
        case "2":
          {
            num = 2;
            pi.add(num);
          }
          break;
        case "3":
          {
            num = 3;
            pi.add(num);
          }
          break;
        case "4":
          {
            num = 4;
            pi.add(num);
          }
          break;
        case "5":
          {
            num = 5;
            pi.add(num);
          }
          break;
        case "6":
          {
            num = 6;
            pi.add(num);
          }
          break;
        case "7":
          {
            num = 7;
            pi.add(num);
          }
          break;
        case "8":
          {
            num = 8;
            pi.add(num);
          }
          break;
        case "9":
          {
            num = 9;
            pi.add(num);
          }
          break;
      }
    } else {
      double right;
      double left;
      right = pi.last;
      pi.removeLast();
      left = pi.last;
      pi.removeLast();
      double te = 0;
      if (num_token[i] == "+") {
        te = (left + right);
      }
      if (num_token[i] == "-") {
        te = (left - right);
      }
      if (num_token[i] == "*") {
        te = (left * right);
      }
      if (num_token[i] == "/") {
        te = (left / right);
      }
      if (num_token[i] == "%") {
        te = (left % right);
      }
      pi.add(te);
    }
  }
  print('Evaluate of postfix : $pi');
}


